<?php
session_start();
foreach ($_POST as $key=>$value){
    $clean_data[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

function trim_value(&$value) 
{ 
    $value = trim($value); 
}
array_walk($clean_data, 'trim_value');
if (empty($clean_data['personname']) || empty($clean_data['family']) ||
        empty($clean_data['username']) || empty($clean_data['email'])) {
     $_SESSION['error'][]="Попълнете задължителните полета !";
     $_SESSION['prev_data']=$clean_data;
   header("Location:../index.php");
}else{


if(!filter_var($clean_data['email'], FILTER_VALIDATE_EMAIL)) {
  $_SESSION['error'][]="Емайла не е валиден !";
  $_SESSION['prev_data']=$clean_data;
   header("Location:../index.php");
} else{

 $_SESSION['clean_data_users'] = $clean_data;
 $_SESSION['prev_data']=$clean_data;
    header("Location:../adress.php");
}
}
