<?php
session_start();
require_once 'connect.php';

if (!empty($_SESSION['notes']) && !empty($_SESSION['clean_data_users']) && !empty($_SESSION['adress'])) {
try{

    $dbh->beginTransaction();

    $stmt = $dbh->prepare("INSERT INTO users (user_fname, user_mname,user_lname,user_login,user_email,user_phone)"
            . " VALUES (:user_fname, :user_mname,:user_lname,:user_login,:user_email,:user_phone)");

    $stmt->bindParam(':user_fname', $_SESSION['clean_data_users']['personname']);
    $stmt->bindParam(':user_mname', $_SESSION['clean_data_users']['fathername']);
    $stmt->bindParam(':user_lname', $_SESSION['clean_data_users']['family']);
    $stmt->bindParam(':user_login', $_SESSION['clean_data_users']['username']);
    $stmt->bindParam(':user_email', $_SESSION['clean_data_users']['email']);
    $stmt->bindParam(':user_phone', $_SESSION['clean_data_users']['phone']);
    $stmt->execute();


    $lastIDuser = $dbh->lastInsertId();


    for ($i = 0; $i < count($_SESSION['adress']['adress1']); $i++) {
        $stmtAdress = $dbh->prepare("INSERT INTO addresses (address_line_1,address_line_2,address_zip,address_city,
            address_province,address_country)"
            . " VALUES (:address_line_1, :address_line_2,:address_zip,:address_city,:address_province,:address_country)");

        $stmtAdress->bindParam(':address_line_1', $_SESSION['adress']['adress1'][$i]);
        $stmtAdress->bindParam(':address_line_2', $_SESSION['adress']['adress2'][$i]);
        $stmtAdress->bindParam(':address_zip', $_SESSION['adress']['postcode'][$i]);
        $stmtAdress->bindParam(':address_city', $_SESSION['adress']['city'][$i]);
        $stmtAdress->bindParam(':address_province', $_SESSION['adress']['region'][$i]);
        $stmtAdress->bindParam(':address_country', $_SESSION['adress']['country'][$i]);
        $stmtAdress->execute();
        $lastIdAdress = $dbh->lastInsertId();

        $stmtAdressUser = $dbh->prepare("INSERT INTO users_addresses (ua_user_id,ua_address_id)"
                . " VALUES (:ua_user_id, :ua_address_id)");

        $stmtAdressUser->bindParam(':ua_user_id', $lastIDuser);
        $stmtAdressUser->bindParam(':ua_address_id', $lastIdAdress);
        $stmtAdressUser->execute();
    }

    for ($i = 0; $i < count($_SESSION['notes']['text']); $i++) {
        $stmtNotes = $dbh->prepare("INSERT INTO notes (note_user_id,note_text)"
                . " VALUES (:note_user_id, :note_text)");
        $stmtNotes->bindParam(':note_user_id', $lastIDuser);
        $stmtNotes->bindParam(':note_text', $_SESSION['notes']['text'][$i]);

        $stmtNotes->execute();
    }

    
    $_SESSION['getLastID'] = $lastIDuser;
    
    $dbh->commit();

        header("Location:../finish.php");

    } catch (PDOException $e){
   $dbh->rollBack();
  echo $e->getMessage();
}

} else {

    $_SESSION['error'] = "Моля прегледайте внимателно всички полета,и се уверете че са попълнени!";
    
    header("Location:../notes.php");
}
    unset($_SESSION['clean_data_users']);
    unset($_SESSION['prev_data']);
    unset($_SESSION['prev_data_adr']);
    unset($_SESSION['adress']);
    unset($_SESSION['notes']);