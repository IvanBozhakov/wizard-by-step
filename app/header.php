<?php session_start();
$active_menu = explode('/',$_SERVER['REQUEST_URI']);
?> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>Wizard StepByStep</title>
<script src="js/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div id="site-container">
    <header><h1>Wizard <b>Step</b>By<b>Step</b></h1>
     <a href="app/destroy.php" class="exit">Изход</a>
            <nav>
                <ul class="nav-bar">
                    <li><a <?php if($active_menu[2]== 'index.php' || empty($active_menu[2])){echo 'class="active"';}?> href="./index.php">Потребителски данни</a></li>
                    <li><a  <?php if($active_menu[2] =='adress.php'){echo 'class="active"';}?> href="./adress.php">Адреси</a></li>
                    <li><a <?php if($active_menu[2] =='notes.php'){echo 'class="active"';}?>href="./notes.php">Бележки</a></li>
                </ul>
            </nav>
       
    <?php
   if(!empty($_SESSION['error'])){
       foreach ($_SESSION['error'] as $err){
           echo '<div class="error">'.$err.'</div>';
       }
    unset($_SESSION['error']);
   }
  
    ?>
    </header>