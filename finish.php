<?php require_once './app/header.php'; 
if(empty($_SESSION['getLastID'])){
   $_SESSION['error'][]=" Попълването на формите беше не успешно моля опитайте отново! ";
header('Location:adress.php');
}
?>
<section>
    <div id="wrapper">
   
     <?php
    require_once 'app/connect.php';
/**
* Get user, address and note date from databese and visualisation
*/
    $getUser = 'SELECT user_id,user_fname,user_mname,user_lname,user_login,user_email,user_phone
    FROM users 
    WHERE user_id=' . $_SESSION['getLastID'] . '';

    $getAddresses = 'SELECT
    address_id,address_line_1,address_line_2,address_zip,address_city,address_province,address_country
    FROM users u JOIN users_addresses us
    ON u.user_id=us.ua_user_id
    JOIN addresses a
    ON a.address_id=us.ua_address_id
    WHERE u.user_id=' . $_SESSION['getLastID'] . '';

    $getNotes = 'SELECT user_id,note_text
    FROM users u 
    JOIN notes
    ON notes.note_user_id=u.user_id
    WHERE u.user_id=' . $_SESSION['getLastID'] . '';

    $user = $dbh->query($getUser);
    $addresses = $dbh->query($getAddresses);
    $notes = $dbh->query($getNotes);


?>

<?php
    foreach ($user as $u) {
        echo '<div class="user"> <fieldset>
  <legend><h4>Потребителски данни</h4></legend>' .
        '<b>Име: </b><p>' . $u['user_fname'] . '</p>' .
        '<b>Бащино име:</b><p> ' . $u['user_mname'] . '</p>' .
        '<b> Фамилия:</b><p>' . $u['user_lname'] . '</p>' .
        '<b>Потребителско име:</b><p> ' . $u['user_login'] . '</p>' .
        '<b>Емайл: </b><p>' . $u['user_email'] . '</p>' .
        '<b>Телефон:</b><p>' . $u['user_phone'] . '</p>' .
        '</fieldset></div>';
    }

?>

<?php
    $address_number=1;
    foreach ($addresses as $a) {
        echo '
  <span class="address"><fieldset><legend><h4>Адрес №'.$address_number.'</h4></legend>' .
        '<b>Адрес 1:</b><p>' . $a['address_line_1'] . '</p>' .
        '<b>Адрес 2:</b><p> ' . $a['address_line_2'] . '</p>' .
        '<b>Пощенски код:</b><p> ' . $a['address_city'] . '</p>' .
        '<b>Област:</b><p>' . $a['address_province'] . '</p>' .
        '<b>Държава: </b><p>' . $a['address_country'] . '</p>' .
        '</fieldset></span>';
        $address_number++;
    }
   
    ?>
    </div><hr>
    <div id="wrapperNotes">
        <?php
         echo '<h4>Бележки</h4>';
       $note_number=1;
    foreach ($notes as $note) {
        echo '<div class="notes"><h4>№'.$note_number.'</h4>' .
        '<p>' . $note['note_text'] . '</p><hr/>' .
        '</div>';
        $note_number=$note_number+1;
    }
        ?>
    </div>
</section>
<?php require_once './app/footer.php'; ?>