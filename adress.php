<?php require_once './app/header.php';
if(empty($_SESSION['clean_data_users'])){
    $_SESSION['error'][]="Попълнете формите и прължете напред ! ";
    header('Location:index.php');
}
?>
<section>
    
    <div class="address-wrapp">
    <form class="form-data" action="app/adrValidation.php" method="POST">
     <fieldset>
  <legend><h4>Добавяне на адрес</h4></legend>
         <label for="adress1">Адрес ред 1*</label><br/>
        <input type="text" name="adress1[]" value="<?php if(!empty($_SESSION['prev_data_adr']['adress1'])){echo $_SESSION['prev_data_adr']['adress1'][0];}?>" /><br/>
        <label for="adress2">Адрес ред 2</label><br/>
        <input type="text" name="adress2[]" value="<?php if(!empty($_SESSION['prev_data_adr']['adress2'])){echo $_SESSION['prev_data_adr']['adress2'][0];}?>" /><br/>
        <label for="postcode">пощенски код*</label><br/>
        <input type="text" class="postcode" name="postcode[]" value="<?php if(!empty($_SESSION['prev_data_adr']['postcode'])){echo $_SESSION['prev_data_adr']['postcode'][0];}?>" /><br/>
        <label for="city">Населено място* </label><br/>
        <input type="text" name="city[]" value="<?php if(!empty($_SESSION['prev_data_adr']['city'])){echo $_SESSION['prev_data_adr']['city'][0];}?>"/><br/>
        <label for="region">Област*</label><br/>
        <input type="text" name="region[]" value="<?php if(!empty($_SESSION['prev_data_adr']['region'])){echo $_SESSION['prev_data_adr']['region'][0];}?>" /><br/>
        <label for="country">Държава</label><br/>
        <input type="text" name="country[]" value="<?php if(!empty($_SESSION['prev_data_adr']['country'])){echo $_SESSION['prev_data_adr']['country'][0];}?>"/><br/>
        <br>
            <?php
            if(!empty($_SESSION['adress'])){
      for($i=0;$i<count($_SESSION['adress']['adress1']);$i++){
         
echo '<div class="otheradress"><h3>Допълнителен адрес</h3><label for="adress1">Адрес ред 1*</label><br/>'.
        '<input type="text" name="adress1[]" '.'value="'.$_SESSION['adress']['adress1'][$i].'"/><br/>'.
        '<label for="adress2">Адрес ред 2</label><br/>'.
        '<input type="text" name="adress2[]" '.'value="'.$_SESSION['adress']['adress2'][$i].'"/><br/>'.
        '<label for="postcode">Пощенски код*</label><br/>'.
        '<input type="text" class="postcode" name="postcode[]" '.'value="'.$_SESSION['adress']['postcode'][$i].'"/><br/>'.
        '<label for="city">Населено място*</label><br/>'.
        '<input type="text" name="city[]" '.'value="'.$_SESSION['adress']['city'][$i].'"/><br/>'.
        '<label for="region">Област*</label><br/>'.
        '<input type="text" name="region[]" '.'value="'.$_SESSION['adress']['region'][$i].'"/><br/>'.
        '<label for="country">Държава</label><br/>'.
        '<input type="text" name="country[]" '.'value="'.$_SESSION['adress']['country'][$i].'"/></div><br>';
         }  
            }
        ?>
        
        <input class="submit" type="submit" value="Продължи" name="submit">
        <input class="add"type="submit" value="Добавяне на нов адрес" name="add">
        </fieldset>
    </form>
    </div>
</section>

<?php require_once './app/footer.php';?>